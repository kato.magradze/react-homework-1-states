import logo from './logo.svg';
import './App.css';
import Card from './Components/Card';
import BackgroundColor from './Components/BackgroundColor';
import { useState } from 'react';
import FontSize from './Components/FontSize';
import AlertButton from './Components/AlertButton';

function App() {
  
  const [radioState, setRadioState] = useState();
  const [fontValue, setFontValue] = useState(16);

  const radioClickHandle = (event, id) => {
    setRadioState(event.target.id);
  }

  const fontChangeHandle = (event) => {
    setFontValue(event.target.value);
  }

  const alertClickHandle = () => {
    alert("ამომეწურა იდეები");
  }

  return (
    <div className={`App App--${radioState}`} style={{fontSize: `${fontValue}px`}} >
      <div className = "App-header__container">
        <BackgroundColor radioClicked={radioClickHandle}/>
        <FontSize fontChanged={fontChangeHandle}/>
      </div>
      <Card/>
      <AlertButton alertClicked={alertClickHandle}/>
    </div>
  );
}

export default App;
