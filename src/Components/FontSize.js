import './FontSize.css'

const FontSize = ({fontChanged}) => {
    return (
        <div class="font-size__container">
            <h2>ADJUST FONT SIZE</h2>
            <input type="number" min="11" max="22" defaultValue="16" onClick={fontChanged}/>
        </div>
    )
}

export default FontSize;