import './Output.css'

const Output = ({text, isDark}) => {
    return (
        <h1 className={`fields__output ${isDark ? "fields__output--darkmode" : ""}`}>Output: {text}</h1>
    )
}

export default Output;