import './AlertButton.css'

const AlertButton = ({alertClicked}) => {
    return (
        <button className="fields__button" onClick={alertClicked}>click here to get a surprise alert!</button>
    )
}

export default AlertButton;