import './Card.css'
import React, { useState } from 'react';
import DarkMode from './DarkMode';
import Textfield from './Textfield';
import Output from './Output';

const Card = () => {

    const [checkboxValue, setCheckboxValue] = useState(false);
    const [textfieldValue, setTextfieldValue] = useState();
    
    const checkboxClickHandler = (event) => {
        setCheckboxValue(event.target.checked);
    }

    const inputHandler = (event) => {
        setTextfieldValue(event.target.value);
    }

    return (
        <div className={`card ${checkboxValue ? "card--dark" : ""}`}>
            <DarkMode checkboxClicked={checkboxClickHandler} isDark={checkboxValue}/>
            <div class="card__fields-container">
                <Textfield inputEntered={inputHandler} isDark={checkboxValue}/>
                <Output text={textfieldValue} isDark={checkboxValue}/>
            </div>
        </div>
    )
}

export default Card;