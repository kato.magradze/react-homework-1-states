import './BackgroundColor.css'

const BackgroundColor = ({radioClicked}) => {
    return (
        <div className="background-color__container">
            <h2>PICK A THEME</h2>
            <div className="background-color">
                <input type="radio" id="green" name="color" onClick={radioClicked}/>
                <label for="green">Forest</label>
                <input type="radio" id="blue" name="color" onClick={radioClicked}/>
                <label for="blue">Sea</label>
                <input type="radio" id="purple" name="color" onClick={radioClicked}/>
                <label for="purple">Potion</label>
                <input type="radio" id="default" name="color" onClick={radioClicked}/>
                <label for="default">Default</label>
            </div>
        </div>
    )
}

export default BackgroundColor;