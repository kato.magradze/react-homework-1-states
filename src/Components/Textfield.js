import './Textfield.css'

const Textfield = ({inputEntered, isDark}) => {
    return (
        <input 
        type="text" 
        className={`fields__textfield ${isDark ? "fields__textfield--darkmode" : ""}`} 
        placeholder="Enter Some text..." 
        onChange={inputEntered}
        />
    )
}

export default Textfield;