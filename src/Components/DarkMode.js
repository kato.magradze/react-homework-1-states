import './DarkMode.css'

const DarkMode = ({checkboxClicked, isDark}) => {
    return (
        <div className="card__darkMode">
            <input type="checkbox" className="checkbox__darkmode" id="darkmode" onClick={checkboxClicked}/>
            <label for="darkmode" class={`checkbox-darkmode__label ${isDark ? "checkbox-darkmode__label--white" : ""}`}>Dark Mode</label>
        </div>
    )
}

export default DarkMode;